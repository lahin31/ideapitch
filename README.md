# IdeaPitch

## Required Tools & Versions

- Node.js => v18.12.1
- Npm => 8.19.2
- MySQL

## How to run Server?

- Create a database inside your mysql localhost called 'ideapitch'.
- Navigate to server folder.
- Copy content from .env.example and create a file called .env and paste inside of it.
- Run `npm install`
- Run `npm start`, you can see ideas, users & funds tables created.
- Run `npx sequelize-cli db:seed:all`, you can see seeder data inside users and ideas tables.

## How to run Client?

- Navigate to client folder.
- Run `npm install`
- Run `npm start`

## For API docs please go to

- http://localhost:4000/idea-api-docs/
- http://localhost:4000/fund-api-docs/
