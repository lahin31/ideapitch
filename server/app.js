const express = require("express");
const swaggerUi = require('swagger-ui-express');
const ideaSwaggerDocument = require('./swaggers/idea-swagger.json');
const fundSwaggerDocument = require('./swaggers/fund-swagger.json');
const db = require("./database");
const middlewares = require("./middlewares");
const router = require("./routes");
const appError = require("./utils/appError");

require("dotenv").config();

const app = express();

app.use(...middlewares);
app.use('/idea-api-docs', swaggerUi.serve,  (...args) => swaggerUi.setup(ideaSwaggerDocument)(...args));
app.use('/fund-api-docs', swaggerUi.serve, (...args) => swaggerUi.setup(fundSwaggerDocument)(...args));

router.registerApplicationRoutes(app);

// Database initialization
db.sequelize.sync().then(
  function () {
    console.log("DB connected successfully!");
  },
  function (err) {
    // handling db connection error
    console.error(err);
  }
);

app.all("*", (req, _, next) => {
  next(new appError(`Cannot find ${req.originalUrl} on this server`, 404));
});

module.exports = app;