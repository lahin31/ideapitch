const express = require("express");
const router = express.Router();
const checkAuth = require('../../middlewares/check-auth');
const fundController = require("../../controllers/fund")

router.get('/', fundController.getFunds);
router.delete('/:fundId', checkAuth, fundController.refundIdea);

module.exports = router;