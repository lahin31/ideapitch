const express = require("express");
const router = express.Router();
const checkAuth = require('../../middlewares/check-auth');
const ideasController = require('../../controllers/idea');
const multer = require("multer");
const upload = multer({
    dest: 'uploads',
    limits: { fieldSize: 25 * 1024 * 1024 },
});

router.get('/', checkAuth, ideasController.fetchIdeas);
router.get('/:slug', ideasController.fetchIdea);
router.post('/', [checkAuth, upload.single("file")], ideasController.postIdea);
router.patch('/:id', checkAuth, ideasController.ideaUpdate);

module.exports = router;