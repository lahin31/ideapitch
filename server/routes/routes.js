module.exports = {
  '/api/users': require('./user'),
  '/api/funds': require('./fund'),
  '/api/ideas': require('./idea')
};
