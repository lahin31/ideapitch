const express = require("express");
const router = express.Router();
const limiter = require("../../middlewares/limiter");
const userAuthController = require("../../controllers/user/auth");

router.get("/logout", userAuthController.logout);
router.get("/reg-user", userAuthController.fetchRegisteredUser);

router.post("/signup", limiter.signUpLimitter, userAuthController.signUp);
router.post("/login", limiter.loginLimitter, userAuthController.login);

module.exports = router;