const Joi = require('joi');

const ideaSchemaValidation = Joi.object({
  title: Joi.string().required(),
  slug: Joi.string().required(),
  description: Joi.string().required(),
  budget: Joi.number().required()
});

module.exports = {
  ideaSchemaValidation
};
