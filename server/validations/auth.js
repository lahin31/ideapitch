const Joi = require('joi');

const userSignUpSchema = Joi.object({
  name: Joi.string().required(),
  username: Joi.string().required(),
  password: Joi.string().required()
});

module.exports = {
  userSignUpSchema
};
