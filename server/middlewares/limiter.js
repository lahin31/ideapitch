const rateLimiter = require('express-rate-limit');

const signUpLimitter = rateLimiter({
  max: 5,
  windowMS: 10000, // 10 seconds
  message: "You can't make any more requests at the moment. Try again later"
});

const loginLimitter = rateLimiter({
  max: 6,
  windowMS: 10000, // 10 seconds
  message: "You can't make any more requests at the moment. Try again later"
});

module.exports = {
  signUpLimitter,
  loginLimitter
};
