const db = require('../../database');
const Idea = db.ideas;
const User = db.users;
const UserService = require('../user');
const FundService = require('../fund');

const fetchIdeas = async (req) => {
    const ideas = await Idea.findAll({
        where: {
            ...req.user.role === "user" && { status: 'approved' }
        },
        include: [{
            model: User,
            as: "user",
            attributes: {
                exclude: ['password', 'updatedAt'] // Removing password from User response data
            } 
        }],
    });
    return ideas;
};

const fetchIdea = async (slug) => {
    const idea = await Idea.findOne({
        where: {
            slug
        },
        include: [{
            model: User,
            as: "user",
            attributes: {
                exclude: ['password', 'updatedAt'] // Removing password from User response data
            } 
        }],
    });

    return {
        idea,
        statusCode: 200
    }
}

const postIdea = async ({ title, slug, description, budget, req }) => {
    const new_post = {
        title,
        slug,
        description,
        budget,
        status: "not_approved",
        userId: req.user.id,
        thumbnail: req.file.filename,
        clap: 0,
        raised_funds: 0
    }

    await Idea.create(new_post);

    return {
        message: "New idea created successfully, an admin will approve",
        statusCode: 201
    }
}

const statusUpdate = async ({ id, status }) => {
    const idea = await Idea.findOne({
        where: {
            id
        }
    });

    await idea.update({ status });

    const ideas = await Idea.findAll({
        include: [{
            model: User,
            as: "user",
            attributes: {
                exclude: ['password', 'updatedAt'] // Removing password from User response data
            } 
        }],
    });

    return {
        message: "Successfully Updated",
        statusCode: 200,
        ideas,
    }
}

const increaseClap = async ({ id, clap, req }) => {
    const idea = await Idea.findOne({
        where: {
            id
        }
    });

    const {validity} = await FundService.validateClaps({ userId: req.user.id, idea_slug: idea.slug })
    // validate the clap should not be exceed 50 times
    if(!validity) {
        return {
            message: "You cannot clap more than 50 at an idea",
            statusCode: 400,
        }
    }
    
    await idea.update({ clap, raised_funds: idea.raised_funds + idea.budget });

    const ideas = await Idea.findAll({
        where: {
            ...req.user.role === "user" && { status: 'approved' }
        },
        include: [{
            model: User,
            as: "user",
            attributes: {
                exclude: ['password', 'updatedAt'] // Removing password from User response data
            } 
        }],
    });

    await UserService.deductUserTotalAmount({ req, ideaBudget: idea.budget });

    await FundService.updateFund({ userId: req.user.id, idea_slug: idea.slug, ideaBudget: idea.budget })

    return {
        message: "Successfully Updated",
        statusCode: 200,
        ideas,
    }
}

module.exports = {
    fetchIdea,
    fetchIdeas,
    statusUpdate,
    postIdea,
    increaseClap
}