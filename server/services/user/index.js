const db = require('../../database');
const User = db.users;

const deductUserTotalAmount = async ({ req, ideaBudget }) => {
    const user = await User.findOne({
        where: {
            id: req.user.id
        }
    });

    await user.update({ total_amount: user.total_amount - ideaBudget });
}

module.exports = {
    deductUserTotalAmount
}