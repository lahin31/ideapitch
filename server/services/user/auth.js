const jwt = require('jsonwebtoken');
const db = require('../../database');
const User = db.users;

const fetchRegisteredUser = async (req) => {
    const auth = jwt.verify(
        req.cookies.token,
        process.env.ACCESS_TOKEN_SECRET
    );

    const user = await User.findOne({ where: { id: auth.id } });
    user.password = undefined; // hide the user password

    return { user }
}

module.exports = {
    fetchRegisteredUser
}