const db = require('../../database');
const Fund = db.funds;
const User = db.users;
const Idea = db.ideas;
const daysUtils = require('../../utils/days');

const getFunds = async (idea_slug) => {
    const funds = await Fund.findAll({
        where: {
            idea_slug: idea_slug
        },
        include: [{
            model: User,
            as: "user",
            attributes: {
                exclude: [
                    'role',
                    'total_amount',
                    'password', 
                    'updatedAt'
                ]
            } 
        }],
    });

    return {
        funds,
        statusCode: 200
    }
}

const updateFund = async ({ userId, idea_slug, ideaBudget }) => {
    const fund = await Fund.findOne({
        where: {
            userId: userId,
            idea_slug
        }
    });

    if(fund) {
        await fund.update({ 
            total_claps: fund.total_claps + 1,
            total_funds: fund.total_funds + ideaBudget
        });
    } else {
        const new_fund = {
            userId,
            idea_slug,
            total_claps: 1,
            total_funds: ideaBudget,
            funding_date: new Date()
        }
        await Fund.create(new_fund);
    }
    return;
}

const validateClaps = async ({ userId, idea_slug }) => {
    const fund = await Fund.findOne({
        where: {
            userId,
            idea_slug
        }
    });

    if(fund?.total_claps === 50) {
        return {
            validity: false
        };
    }

    return {
        validity: true
    };
}

const refundIdea = async ({ fundId, idea_slug, user_id }) => {
    const fund = await Fund.findOne({
        where: {
            id: fundId
        }
    });

    if(daysUtils.differenceInDays(fund.funding_date, new Date()) <= 7) {
        await Fund.destroy({
            where: {
                id: fundId
            }
        });

        const user = await User.findOne({
            where: {
                id: user_id
            }
        });

        await user.update({ total_amount: parseInt(user.total_amount) + parseInt(fund.total_funds) });

        const idea = await Idea.findOne({
            where: {
                slug: idea_slug
            }
        });

        await idea.update({ 
            raised_funds: idea.raised_funds - fund.total_funds,
            clap: idea.clap - fund.total_claps
        });

        // const { idea: ideaData } = await IdeaService.fetchIdea(idea_slug);
        const ideaData = await Idea.findOne({
            where: {
                slug: idea_slug
            },
            include: [{
                model: User,
                as: "user",
                attributes: {
                    exclude: ['password', 'updatedAt'] // Removing password from User response data
                } 
            }],
        });

        const { funds } = await getFunds(idea_slug);

        return {
            idea: ideaData,
            funds,
            statusCode: 200
        }
    }
}

module.exports = {
    getFunds,
    updateFund,
    refundIdea,
    validateClaps
}