const UsersSeederData = [
  {
    name: 'Anwar Hussain',
    username: 'anwar',
    total_amount: 10000,
    role: 'admin',
    password: '$2a$12$ZclYc14SQEscUE3cBMwej.JA9GVvfNY/Hw2sBBobUTmk8Ys2IWCny'
  },
  {
    name: 'Muhammad Lahin',
    username: 'lahin',
    total_amount: 10000,
    role: 'user',
    password: '$2a$12$ZclYc14SQEscUE3cBMwej.JA9GVvfNY/Hw2sBBobUTmk8Ys2IWCny'
  },
  {
    name: 'Sadikul Islam',
    username: 'sadik',
    total_amount: 10000,
    role: 'user',
    password: '$2a$12$ZclYc14SQEscUE3cBMwej.JA9GVvfNY/Hw2sBBobUTmk8Ys2IWCny'
  },
  {
    name: 'Rubel',
    username: 'rubel',
    total_amount: 10000,
    role: 'user',
    password: '$2a$12$ZclYc14SQEscUE3cBMwej.JA9GVvfNY/Hw2sBBobUTmk8Ys2IWCny'
  }
];

module.exports = {
  UsersSeederData
};
