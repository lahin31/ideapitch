const IdeasSeederData = [
  {
    title: 'This is my first idea',
    slug: 'first_idea',
    description:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
    budget: 100,
    raised_funds: 0,
    clap: 0,
    status: 'not_approved',
    userId: 2
  },
  {
    title: 'My awesome post',
    slug: 'awesome_post',
    description:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
    budget: 100,
    raised_funds: 0,
    clap: 0,
    status: 'not_approved',
    userId: 3
  },
  {
    title: 'JavaScript is awesome',
    slug: 'awesome_js',
    description:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
    budget: 100,
    raised_funds: 0,
    clap: 0,
    status: 'not_approved',
    userId: 3
  }
];

module.exports = {
  IdeasSeederData
};
