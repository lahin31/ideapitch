const catchAsync = require("../../utils/catchAsync");
const IdeaService = require("../../services/idea");
const IdeaValidation = require("../../validations/idea");

const fetchIdeas = catchAsync(async (req, res) => {
    const ideas = await IdeaService.fetchIdeas(req);

    res.status(200).json({
        ideas
    });
})

const postIdea = catchAsync(async (req, res) => {
    const { title, slug, description, budget } = req.body;

    const { error } = IdeaValidation.ideaSchemaValidation.validate(req.body);

    if(error && error.details && error.details.length > 0) {
      return res.status(409).json({ error: error.details })
    }

    const { message, statusCode } = await IdeaService.postIdea({ title, slug, description, budget, req })

    res.status(statusCode).json({
        message
    });
});

const ideaUpdate = catchAsync(async (req, res) => {
    const { id } = req.params;
    const { type } = req.body;

    if(type === "make-approve") {
        const { status } = req.body;
        const { message, ideas, statusCode } = await IdeaService.statusUpdate({ id, status });
        
        res.status(statusCode).json({
            message,
            ideas
        });
    } else if(type === "increase-clap") {
        const { clap } = req.body;
        const response = await IdeaService.increaseClap({ id, clap, req });

        if(response.statusCode === 400) {
            return res.status(response.statusCode).json({
                message: response.message
            });
        }

        res.status(response.statusCode).json({
            message: response.message,
            ideas: response.ideas
        });
    }
});

const fetchIdea = catchAsync(async (req, res) => {
    const { slug } = req.params;

    const { idea, statusCode } = await IdeaService.fetchIdea(slug, req);

    res.status(200).json({
        idea,
        statusCode
    });
})

module.exports = {
    postIdea,
    fetchIdea,
    fetchIdeas,
    ideaUpdate
}