const catchAsync = require("../../utils/catchAsync");
const FundService = require('../../services/fund');

const getFunds = catchAsync(async (req, res) => {
    const { idea_slug } = req.query;

    const { funds, statusCode } = await FundService.getFunds(idea_slug);

    res.status(statusCode).json({
        funds
    });
});

const refundIdea = catchAsync(async (req, res) => {
    const { fundId } = req.params;
    const { idea_slug, user_id } = req.query;
    
    const { funds, idea, statusCode } = await FundService.refundIdea({ fundId, idea_slug, user_id });

    res.status(statusCode).json({
        funds,
        idea
    });
});

module.exports = {
    getFunds,
    refundIdea
}