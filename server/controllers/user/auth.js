const bcrypt = require('bcryptjs');
const catchAsync = require('../../utils/catchAsync');
const AppError = require('../../utils/appError');
const UserAuthService = require('../../services/user/auth');
const authValidation = require('../../validations/auth');
const objectUtil = require('../../utils/object');
const passwordHelper = require('../../helpers/password');
const tokenHelper = require('../../helpers/token');
const db = require('../../database');
const User = db.users;

const fetchRegisteredUser = catchAsync(async (req, res, next) => {
  if (!req?.cookies.token) {
    return next(new AppError('Cookies not found!'));
  }

  const { user } = await UserAuthService.fetchRegisteredUser(req);

  res.status(200).json({
    user,
  });
});

const signUp = catchAsync(async (req, res, next) => {
  const { name, username, password } = req.body;

  const { error } = authValidation.userSignUpSchema.validate(req.body);

  if(error && error.details && error.details.length > 0) {
    return res.status(409).json({ error: error.details })
  }

  const existedUser = await User.findOne({
    where: { username }
  }) || {};

  if(objectUtil.objectEmpty(existedUser)) {
    return next(new AppError('The user is already registered!', 400));
  }

  const hashedPassword = await bcrypt.hash(password, 12);

  const user = {
    name,
    username,
    password: hashedPassword,
    total_amount: 10000,
    role: "user"
  }

  await User.create(user);

  res.status(201).json({
    message: 'User registered successfully',
    statusCode: 201,
  });
});

const login = catchAsync(async (req, res, next) => {
  const { username, password } = req.body;
    
  const existedUser = await User.findOne({
    where: { username }
  }) || {};

  if(!existedUser) {
    return next(new AppError('Incorrect username or password', 401));
  }

  const isMatch = await passwordHelper.matchPassowrd(password, existedUser.password);

  if (!isMatch)
    return next(new AppError('Incorrect username or password!', 401));

  createSendToken(existedUser, 200, res);
});

const logout = (_, res) => {
  return res.status(202).clearCookie('token').send({
    message: 'Token deleted',
  });
};

const createSendToken = (user, statusCode, res) => {
  const token = tokenHelper.generateAccessToken(user.id);
  const timeLimit = 31536000000; // one year

  const cookieOptions = {
    expires: new Date(Date.now() + timeLimit),
    httpOnly: true,
    // secure: process.env.NODE_ENV === 'production' ? true : false,
    secure: false,
  };

  res.cookie('token', token, cookieOptions);

  user.password = undefined; // hide the user password

  res.status(statusCode).json({
    status: 'success',
    statusCode,
    user,
    token
  });
}

module.exports = {
  signUp,
  login,
  logout,
  fetchRegisteredUser
}