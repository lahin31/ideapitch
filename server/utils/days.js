const differenceInDays = (d1, d2) => {
  const difference_in_time = d2.getTime() - d1.getTime();

  const difference_in_days = difference_in_time / (1000 * 3600 * 24);

  return difference_in_days;
};

module.exports = {
  differenceInDays
};
