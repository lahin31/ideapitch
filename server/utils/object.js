const objectEmpty = obj => Object.keys(obj) && Object.keys(obj).length > 0;

module.exports = { objectEmpty };
