module.exports = (sequelize, DataTypes) => {
  const Fund = sequelize.define(
    'fund',
    {
      idea_slug: {
        type: DataTypes.STRING(20)
      },
      total_claps: {
        type: DataTypes.INTEGER
      },
      total_funds: {
        type: DataTypes.INTEGER
      },
      funding_date: {
        type: DataTypes.DATE
      }
    },
    {
      timestamps: false
    }
  );
  return Fund;
};
