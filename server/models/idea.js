module.exports = (sequelize, DataTypes) => {
  const Idea = sequelize.define(
    'idea',
    {
      title: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          max: {
            args: [20],
            msg: 'Maximum 20 characters allowed in title'
          }
        }
      },
      slug: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          max: {
            args: [12],
            msg: 'Maximum 12 characters allowed in username'
          }
        }
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      budget: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      raised_funds: {
        type: DataTypes.INTEGER
      },
      thumbnail: {
        type: DataTypes.STRING
      },
      clap: {
        type: DataTypes.INTEGER(50)
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      timestamps: false
    }
  );

  return Idea;
};
