module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'user',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          max: {
            args: [30],
            msg: 'Maximum 30 characters allowed in name'
          }
        }
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          max: {
            args: [12],
            msg: 'Maximum 12 characters allowed in username'
          }
        }
      },
      total_amount: {
        type: DataTypes.DECIMAL
      },
      password: {
        type: DataTypes.STRING(200),
        allowNull: false
      },
      role: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      timestamps: false
    }
  );

  return User;
};
