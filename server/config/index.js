module.exports = {
  app: {
    name: 'IdeaPitch',
    env: process.env.APP_ENV,
    secret: process.env.APP_SECRET,
    port: process.env.APP_PORT,
    domain: process.env.APP_DOMAIN
  },
  db: {
    mysql: {
      username: process.env.DB_USER || 'root',
      password: process.env.DB_PASS || '',
      host: process.env.DB_HOST || 'localhost',
      port: process.env.DB_PORT,
      dbname: process.env.DB_NAME || 'ideapitch',
      dialect: process.env.DIALECT || 'mysql',
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    }
  }
};
