import { useEffect, useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import { Container } from '@chakra-ui/react';
import Login from 'pages/authentications/Login';
import PrivateRoute from 'components/PrivateRoute/PrivateRoute';
import { isLoggedIn } from './utils/user';
import AuthContext from 'contexts/auth-context';
import Home from 'pages/home/Home';
import Registration from 'pages/authentications/Registration';
import Navbar from 'components/Navbar/Navbar';
import CreateIdea from 'pages/ideas/CreateIdea';
import Idea from 'pages/ideas/Idea';
import { fetchRegisteredUser } from 'api/user';
import ErrorBoundary from 'helpers/ErrorBoundary';

function App() {
  const [user, setUser] = useState({});
  const [userIsLoggedIn, setUserIsLoggedIn] = useState(isLoggedIn());

  useEffect(() => {
    fetchRegisteredUser()
      .then(res => {
        setUser(res.data.user);
        if (res.data.user && Object.keys(res.data.user).length > 0) {
          setUserIsLoggedIn(true);
          localStorage.setItem('user', JSON.stringify(res.data.user));
        }
      })
      .catch(() => console.log("Something went wrong"))
  }, []);

  const login = loggedInUser => {
    setUser(loggedInUser);
    setUserIsLoggedIn(true);
  };

  const logout = () => {
    setUserIsLoggedIn(false);
    localStorage.removeItem('user');
  };

  const contextValue = {
    user,
    login,
    logout,
    userIsLoggedIn
  };

  return (
    <AuthContext.Provider value={{ contextValue }}>
      {userIsLoggedIn && <Navbar role={user.role} />}
      <Container maxW="container.sm">
        <Routes>
          <Route
            path="/"
            element={
              <PrivateRoute>
                <ErrorBoundary>
                  <Home />
                </ErrorBoundary>
              </PrivateRoute>
            }
          />
          <Route
            path="/ideas/:slug"
            element={
              <ErrorBoundary>
                <Idea />
              </ErrorBoundary>
            }
          />
          <Route
            path="/create-idea"
            element={
              <PrivateRoute>
                <ErrorBoundary>
                  <CreateIdea />
                </ErrorBoundary>
              </PrivateRoute>
            }
          />
          <Route path="/login" element={<ErrorBoundary><Login /></ErrorBoundary>} />
          <Route path="/registration" element={<ErrorBoundary><Registration /></ErrorBoundary>} />
        </Routes>
      </Container>
    </AuthContext.Provider>
  );
}

export default App;
