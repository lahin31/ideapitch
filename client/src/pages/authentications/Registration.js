import {
    FormControl,
    FormLabel,
    Box,
    Flex,
    Input,
    Button
} from '@chakra-ui/react'
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axiosInstance from 'services/axiosInstance';

function Registration() {
    const [formData, setFormData] = useState({
        name: "",
        username: "",
        password: ""
    });
    const navigate = useNavigate();

    const handleSubmit = e => {
        e.preventDefault();
        axiosInstance.post("/api/users/signup", {
            name: formData.name,
            username: formData.username,
            password: formData.password
        })
            .then(res => {
                if(res.data.statusCode === 201) {
                    navigate('/login');
                }
            })
    }

    const handleFieldChange = e => {
        setFormData({
          ...formData,
          [e.target.name]: e.target.value
        });
    };

    return (
        <div className="login-wrapper">
            <Flex
                flexDirection="column"
                width="100wh"
                height="100vh"
                justifyContent="center"
                alignItems="center"
            >
                <Box p={5} m={10} shadow='md' borderWidth='1px'>
                    <form onSubmit={handleSubmit}>
                        <FormControl>
                            <FormLabel>Name</FormLabel>
                            <Input 
                                type='text' 
                                placeholder='Name'
                                name="name"
                                value={formData.name}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Username</FormLabel>
                            <Input
                                type='text' 
                                placeholder='Username'
                                name="username"
                                value={formData.username}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <FormControl mt={3}>
                            <FormLabel>Password</FormLabel>
                            <Input 
                                type='password' 
                                placeholder='Password'
                                name="password"
                                value={formData.password}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <Button 
                            borderRadius={5}
                            type="submit"
                            variant="solid"
                            colorScheme="teal"
                            width="full" 
                            mt="7">
                            Register
                        </Button>
                    </form>
                </Box>
            </Flex>
        </div>
    )
}

export default Registration;