import {
    FormControl,
    FormLabel,
    Box,
    Flex,
    Input,
    Button,
} from '@chakra-ui/react';
import AuthContext from 'contexts/auth-context';
import { useContext, useState } from 'react';
import { Link, useNavigate } from "react-router-dom";
import axiosInstance from 'services/axiosInstance';

function Login() {
    const [formData, setFormData] = useState({
        username: '',
        password: ''
    });
    const authContext = useContext(AuthContext);
    const navigate = useNavigate();

    const handleSubmit = e => {
        e.preventDefault();
        axiosInstance.post('/api/users/login', {
          username: formData.username,
          password: formData.password
        }).then(res => {
          if (res.data.statusCode === 200) {
            localStorage.setItem("user", JSON.stringify(res.data.user));
            authContext.contextValue.login(res.data.user);
            navigate("/");
          }
        })
   };
    
    const handleFieldChange = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    return (
        <div className="login-wrapper">
            <Flex
                flexDirection="column"
                width="100wh"
                height="100vh"
                justifyContent="center"
                alignItems="center"
            >
                <Box p={5} m={10} shadow='md' borderWidth='1px'>
                    <form onSubmit={handleSubmit}>
                        <FormControl>
                            <FormLabel>Username</FormLabel>
                            <Input 
                                type='text'
                                name='username'
                                value={formData.username}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <FormControl mt={3}>
                            <FormLabel>Password</FormLabel>
                            <Input 
                                type='password'
                                name='password'
                                value={formData.password}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <Button 
                            borderRadius={5}
                            type="submit"
                            variant="solid"
                            colorScheme="teal"
                            width="full" 
                            mt="7">
                            Login
                        </Button>
                    </form>
                </Box>
                <Box>
                    Don't have an account?{" "}
                    <Link style={{ color: "#2C7A7B" }} to="/registration">
                        Register
                    </Link>
                </Box>
            </Flex>
        </div>
    )
}

export default Login