import { Box, Container, Heading, HStack, Text } from "@chakra-ui/react";
import Funds from "components/Idea/Funds/Funds";
import useFetchFunds from "hooks/use-fetch-funds";
import UseFetchIdea from "hooks/use-fetch-idea";
import { useParams } from "react-router-dom";
import axiosInstance from "services/axiosInstance";

function Idea() {
    const { slug } = useParams();
    const [idea, setIdea] = UseFetchIdea(slug);
    const [funds, setFunds] = useFetchFunds(idea);

    const handleRefund = (fund_id, user_id) => {
        axiosInstance.delete(`/api/funds/${fund_id}?idea_slug=${idea.slug}&user_id=${user_id}`)
            .then(res => {
                setIdea(res.data.idea)
                setFunds(res.data.funds);
            })
    }

    return (
        <Container maxW='container.sm'>
            <Box p="5" borderWidth='1px' borderRadius='lg' overflow='hidden' mt="7">
                <Heading as="h3">{ idea.title }</Heading> by <strong>{ idea.user.name }</strong>
                <hr />
                <Text mt="3">{idea.description}</Text>
                <HStack mt="5">
                    <Text><strong>Budget:</strong> $ {idea.budget}</Text> 
                </HStack>
                <HStack mt="5">
                    <Text><strong>Total Raised Funds:</strong> {idea.raised_funds} $</Text> 
                </HStack>
                <HStack mt="5">
                    <Text><strong>Claps:</strong> {idea.clap}</Text> 
                </HStack>
            </Box>
            <Funds funds={funds} handleRefund={handleRefund} />
        </Container>
    )
}

export default Idea;