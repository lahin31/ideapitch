import { Box, Button, CheckboxIcon, Flex, FormControl, FormLabel, Input, InputGroup, InputLeftElement, InputRightElement, Textarea, useToast } from "@chakra-ui/react";
import { useState } from "react";
import axiosInstance from "services/axiosInstance";

function CreateIdea() {
    const [formData, setFormData] = useState({
        title: "",
        slug: "",
        description: "",
        budget: ""
    });
    const [file, setFile] = useState("");
    const toast = useToast();

    const handleFileChange = e => {
      setFile(e.target.files[0]);
    }

    const handleFieldChange = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleCreateIdea = e => {
        e.preventDefault();

        const _formData = new FormData();
        _formData.append("title", formData.title);
        _formData.append("slug", formData.slug);
        _formData.append("description", formData.description);
        _formData.append("budget", formData.budget);
        _formData.append("file", file);

        axiosInstance.post('/api/ideas', _formData, {
            headers: {
              "Content-type": "multipart/form-data"
            }
        })
        .then(res => {
            if(res.status === 201) {
                makeFieldsEmpty();
                toast({
                    title: res.data.message,
                    position: "bottom",
                });
            }
        })
        .catch(err => console.log(err))    
    }

    const makeFieldsEmpty = () => {
        setFormData({
            title: "",
            slug: "",
            description: "",
            budget: ""
        })
    }

    return (
        <div className="create-idea-wrapper">
            <Flex
                flexDirection="column"
                width="100wh"
                height="100vh"
                justifyContent="center"
                alignItems="center"
            >
                <Box p={5} m={10} shadow='md' borderWidth='1px'>
                    <form onSubmit={handleCreateIdea}>
                        <FormControl>
                            <FormLabel>Title</FormLabel>
                            <Input
                                type='text'
                                name='title'
                                placeholder="Title"
                                value={formData.title}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Slug</FormLabel>
                            <Input
                                type='text'
                                name='slug'
                                placeholder="Slug"
                                value={formData.slug}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Description</FormLabel>
                            <Textarea
                                name='description'
                                placeholder="Description"
                                value={formData.description}
                                onChange={handleFieldChange}
                                required 
                            />
                        </FormControl>
                        <InputGroup mt={5}>
                            <InputLeftElement
                            pointerEvents='none'
                            color='gray.300'
                            fontSize='1.2em'
                            children='$'
                            />
                            <Input placeholder='Budget' name="budget" value={formData.budget} onChange={handleFieldChange} />
                            <InputRightElement children={<CheckboxIcon color='green.500' />} />
                        </InputGroup>
                        <FormControl mt={5}>
                            <FormLabel>Thumbnail</FormLabel>
                            <input type="file" accept="image/*" onChange={handleFileChange} />
                        </FormControl>
                        <Button
                            borderRadius={5}
                            type="submit"
                            variant="solid"
                            colorScheme="teal"
                            width="full" 
                            mt="7">
                            Create Post
                        </Button>
                    </form>
                </Box>
            </Flex>
        </div>
    )
}

export default CreateIdea;