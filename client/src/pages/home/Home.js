import { Container, Heading, useToast } from "@chakra-ui/react";
import AdminIdea from "components/Idea/Admin/Idea";
import UserIdea from "components/Idea/User/Idea";
import AuthContext from "contexts/auth-context";
import useFetchIdeas from "hooks/use-fetch-ideas";
import { useContext } from "react";
import axiosInstance from "services/axiosInstance";

const ROLE_VIEWS = {
    admin: AdminIdea,
    user: UserIdea
}

function Home() {
    const toast = useToast();
    const [ideas, setIdeas] = useFetchIdeas();
    const context = useContext(AuthContext);
    const RoleSpecificView = ROLE_VIEWS[context.contextValue.user.role] ?? UserIdea;

    const makeApprove = id => {
        axiosInstance.patch(`/api/ideas/${id}`, {
            type: "make-approve",
            status: "approved"
        })
            .then(res => {
                if(res.data.ideas && res.data.ideas.length > 0) {
                    setIdeas(res.data.ideas);
                    toast({
                        title: res.data.message,
                        position: "top",
                        isClosable: true,
                    })
                }
            })
            .catch(() => console.log('something went wrong'))
    }

    const handleClap = id => {
        const idea = ideas.find(item => item.id === id);

        axiosInstance.patch(`/api/ideas/${id}`, {
            type: "increase-clap",
            clap: idea.clap + 1
        })
            .then(res => {
                setIdeas(res.data.ideas)
            })
            .catch(() => console.log('something went wrong'))
    }

    return (
        <Container maxW='container.sm'>
            { ideas && ideas.length <= 0 && <Heading as="h2">No ideas available</Heading> }
            { ideas && ideas.length > 0 && ideas.map(idea => {
                return <RoleSpecificView 
                    key={idea.id} 
                    idea={idea} 
                    makeApprove={makeApprove}
                    handleClap={handleClap}
                />
            })}
        </Container>
    )
}

export default Home;