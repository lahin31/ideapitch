import axiosInstance from 'services/axiosInstance';

export const fetchRegisteredUser = () => {
  return axiosInstance.get('/api/users/reg-user');
};
