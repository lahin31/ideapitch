import axiosInstance from 'services/axiosInstance';

export const fetchIdeas = () => {
  return axiosInstance.get('/api/ideas');
};

export const fetchIdea = slug => {
  return axiosInstance.get(`/api/ideas/${slug}`);
};
