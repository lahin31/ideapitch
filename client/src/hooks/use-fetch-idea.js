import { fetchIdea } from 'api/idea';
import { useEffect, useState } from 'react';

const IdeaInitialValue = {
  title: '',
  slug: '',
  description: '',
  budget: '',
  user: {
    name: ''
  }
};

const UseFetchIdea = slug => {
  const [idea, setIdea] = useState(IdeaInitialValue);

  useEffect(() => {
    fetchIdea(slug).then(res => {
      setIdea(res.data.idea);
    });
  }, [slug]);

  return [idea, setIdea];
};

export default UseFetchIdea;
