import { useEffect, useState } from 'react';
import axiosInstance from 'services/axiosInstance';

const useFetchFunds = idea => {
  const [funds, setFunds] = useState([]);

  useEffect(() => {
    axiosInstance.get(`/api/funds?idea_slug=${idea.slug}`)
        .then(res => {
            setFunds(res.data.funds);
        })
        .catch(() => console.log("Something went wrong"));
}, [idea.slug])

  return [funds, setFunds];
};

export default useFetchFunds;
