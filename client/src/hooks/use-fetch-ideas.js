import { fetchIdeas } from 'api/idea';
import { useEffect, useState } from 'react';

const useFetchIdeas = url => {
  const [ideas, setIdeas] = useState([]);

  useEffect(() => {
    fetchIdeas().then(res => {
      setIdeas(res.data.ideas);
    });
  }, [url]);

  return [ideas, setIdeas];
};

export default useFetchIdeas;
