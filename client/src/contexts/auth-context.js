import { createContext } from 'react';

const AuthContext = createContext({
  user: {},
  userIsLoggedIn: false,
  login: user => {},
  logout: () => {}
});

export default AuthContext;
