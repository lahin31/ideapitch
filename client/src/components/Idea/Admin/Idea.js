import { Box, Button, Heading, HStack, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

function AdminIdea({ idea, makeApprove }) {
    const navigate = useNavigate();

    const goToIdeaDetails = slug => {
        navigate(`/ideas/${slug}`);
    }

    return (
        <div className="idea">
            <Box p="5" borderWidth='1px' borderRadius='lg' overflow='hidden' mt="7">
                <Heading as="h3" onClick={() => goToIdeaDetails(idea.slug)}>{ idea.title }</Heading> by <strong>{ idea.user.name }</strong>
                <hr />
                <Text mt="3">{idea.description}</Text>
                <HStack mt="5">
                    <Text><strong>Budget:</strong> $ {idea.budget}</Text> 
                </HStack>
                <HStack mt="5">
                    <Text><strong>Total Raised Funds:</strong> {idea.raised_funds} $</Text> 
                </HStack>
                <HStack mt="5">
                    <Text><strong>Claps:</strong> {idea.clap}</Text> 
                </HStack>
                { idea.status === "not_approved" && <Button mt="5" onClick={() => makeApprove(idea.id)}>Approve</Button>}
            </Box>
        </div>
    )
}

export default AdminIdea;