import { Button, Table, TableContainer, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/react";
import AuthContext from "contexts/auth-context";
import { useContext } from "react";
import { formatDate } from "utils/date";

function Funds({ funds, handleRefund }) {
    const context = useContext(AuthContext);

    return (
        <TableContainer mt="10">
            <Table variant='simple'>
                <Thead>
                    <Tr>
                        <Th>Name</Th>
                        <Th>Total Funds</Th>
                        <Th>Funding Date</Th>
                        <Th>Action</Th>
                    </Tr>
                </Thead>
                <Tbody>
                    {funds.map(fund => {
                        return <Tr key={fund.id}>
                            <Td>{fund.user.name}</Td>
                            <Td>$ {fund.total_funds}</Td>
                            <Td>{formatDate(fund.funding_date)}</Td>
                            {context.contextValue.user.id === fund.user.id && <Td><Button onClick={() => handleRefund(fund.id, fund.user.id)}>Refund</Button></Td>}
                        </Tr>
                    })}
                </Tbody>
            </Table>
        </TableContainer>
    )
}

export default Funds;