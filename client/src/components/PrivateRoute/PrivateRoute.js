import AuthContext from "contexts/auth-context";
import { useContext } from "react";
import { Navigate, useLocation } from "react-router-dom";

const PrivateRoute = ({ children, ...props }) => {
  const { contextValue } = useContext(AuthContext);
  const location = useLocation();

  if (!contextValue.userIsLoggedIn) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  }

  return children;
};

export default PrivateRoute;