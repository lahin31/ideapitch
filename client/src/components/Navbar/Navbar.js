import AuthContext from "contexts/auth-context";
import { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import axiosInstance from "services/axiosInstance";
import "./Navbar.css"

function Navbar() {
    const context = useContext(AuthContext);
    const navigate = useNavigate();

    const handleLogout = () => {
        axiosInstance.get('/api/users/logout')
            .then(() => {
                navigate("/login");
                context.contextValue.logout();
            })
    }

    return (
        <nav>
            <ul>
                <li><Link to="/">Home</Link></li>
                {context.contextValue.user.role === "user" && <li><Link to="/create-idea">Create Idea</Link></li>}
                <li style={{float:"right", marginTop: "12px", cursor: "pointer"}}><span className="active logout-btn" onClick={handleLogout}>Logout</span></li>
            </ul>
        </nav>
    )
}

export default Navbar;